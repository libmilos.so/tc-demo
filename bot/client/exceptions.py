class BaseClientException(Exception):
    pass

class InvalidCredentialsException(BaseClientException):
    pass

class UnauthorizedException(BaseClientException):
    pass
