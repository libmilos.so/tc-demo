import sys
import json
import requests
import random

from client import DemoClient
from bacon import get_a_bacon


class DemoTester:

    def __init__(self, conf):
        # Will raise ValueError if something's wrong
        self.verify_conf(conf)
        self.config = conf


    def verify_conf(self, conf):
        """
        Here we verify config validity. Ugly.
        """
        if len(conf['test_users']) < conf['number_of_users']:
            raise ValueError("Please provide enough test users in config.json - "
                "provided %d, needed %d" % (len(conf['test_users']), conf['number_of_users']))

        getvals = lambda l, key: [x[key].lower() for x in l]

        users = getvals(conf['test_users'], 'user')
        if len(users) != len(set(users)):
            raise ValueError("There are conflicting usernames in config.json"
                " - they all must be unique!")

        emails = getvals(conf['test_users'], 'email')
        if len(users) != len(set(users)):
            raise ValueError("There are conflicting emails in config.json"
                " - they all must be unique!")


    def _register_user(self, user, passwd, email):
        req = requests.post(DemoClient.API_URL + '/accounts/sign-up', {
            'username': user,
            'password': passwd,
            'email': email
        })
        data = req.json()

        if req.status_code == 201:
            client = {
                'client': DemoClient(user, passwd),
                'id': data['id'],
                'posts': 0,
                'likes': 0,
            }
            self._users.append(client)
            return client

        if 'email' in data:
            raise ValueError('User: %s, error: %s' % (user, data['email']))
        if 'username' in data:
            raise ValueError('User: %s, error: %s' % (user, data['username']))
        if 'password' in data:
            raise ValueError('User: %s, error: %s' % (user, data['password']))





    def register_users(self):
        self._users = []

        limit = self.config['number_of_users']
        success = 0

        for user in self.config['test_users']:
            try:
                self._register_user(**user)
                success += 1
            except ValueError as e:
                print(e)

        if success < limit:
            raise ValueError('Not enough valid users provided - valid %s, '
                'needed %s' % (success, limit))


    def post_spam(self):
        max_posts = self.config['max_posts_per_user']
        # Init bacon generator
        bacons = get_a_bacon()
        # We don't want specific users, so we post by a random user
        random.shuffle(self._users)
        for user in self._users:
            # and we post random amount of posts
            count = random.choice(range(1, max_posts))
            for _ in range(count):
                # fetch some bacon
                text = next(bacons)
                user['client'].Posts.new(text)
                user['posts'] += 1


    def like_spam(self):
        max_likes = self.config['max_likes_per_user']

        # Sort users according to their number of posts (high -> low)
        self._users = sorted(self._users, key=lambda u: u['posts'], reverse=True)

        for user in self._users:
            posts = user['client'].Posts.all()

            # Sort posts according to like count
            posts = sorted(posts, key=lambda p: p['num_likes'])

            # Pop posts by self
            posts = [p for p in posts if p['created_by']['id'] != user['id']]

            # Keep only posts with 0 likes
            posts = [p for p in posts if p['num_likes'] == 0]

            if len(posts) == 0:
                print('No more posts with zero likes')
                return

            # Now we have only posts with 0 likes. We can search all posts by those posters now
            # Doing list(set(L)) in order to remove duplicates
            posters = list(set([post['created_by']['id'] for post in posts]))
            # Let's keep them in random order
            random.shuffle(posters)

            posts_to_like = []
            for poster in posters:
                # Fetch new posts as long as we don't collect big enough post pool
                ps = user['client'].Posts.by_user(poster)
                posts_to_like += ps

                if len(posts_to_like) > max_likes:
                    # We have enough posts
                    break

            # Shuffle post pool as well
            random.shuffle(posts_to_like)
            for i in range(max_likes):
                try:
                    user['client'].Posts.like(posts_to_like[i]['id'])
                except IndexError:
                    # Not enough posts available. Oh well.
                    break

        print('Reached maximum likes on all users')



    def run(self):
        # Let's register the users first
        try:
            self.register_users()
        except ValueError as e:
            print(e)
            sys.exit(1)

        # Post all the posts!
        self.post_spam()

        # And now, the like spam!
        self.like_spam()



if __name__ == '__main__':

    with open('./config.json') as f:
        conf = json.load(f)

    try:
        dt = DemoTester(conf)
    except ValueError as e:
        print(e)
        sys.exit(1)

    dt.run()


