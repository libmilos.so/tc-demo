from django.contrib.auth import get_user_model
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import status

from .serializers import UserSerializer, ProfileSerializer


@api_view(['GET'])
def profile(request):
    sz = ProfileSerializer(request.user.profile)
    return Response(sz.data)


@api_view(['POST'])
@permission_classes((AllowAny, ))
def register(request):
    if request.user.is_authenticated():
        return Response({'error': 'You already have an account.'},
            status=status.HTTP_403_FORBIDDEN)

    sz = UserSerializer(data=request.data)

    if sz.is_valid():
        sz.save()
        return Response(sz.data, status=status.HTTP_201_CREATED)
    return Response(sz.errors, status=status.HTTP_400_BAD_REQUEST)


# @api_view(['PUT'])
# def change_password(request):
#     old = request.data.get('old-password')
#     new = request.data.get('new-password')
