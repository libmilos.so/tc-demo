from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from celery import shared_task
from celery.contrib import rdb
from requests.exceptions import HTTPError
import clearbit

from .models import Profile


# We init the key here because it's library global
# Other way would be to have it as CLEARBIT_API_KEY
# environ variable for celery workers.
clearbit.key = settings.CLEARBIT_API_KEY


@shared_task(ignore_result=True)
def send_welcome_mail(uid):
    user = get_user_model().objects.get(pk=uid)
    send_mail(
        'Welcome to Demo!', # subject
        'Welcome to Demo social network, enjoy your stay!', # body
        None, # from address, if None, uses one from config
        [user.email], # dest address
    )


@shared_task(ignore_result=True)
def init_profile(uid):
    """
    We're building a small background on our user
    through a 3rd party service here.

    For more info regarding their API,
    visit https://dashboard.clearbit.com/docs
    """
    user = get_user_model().objects.get(pk=uid)
    profile = Profile.objects.get(user=user)

    try:
        lp = clearbit.Enrichment.find(email=user.email, stream=True)
    except HTTPError as e:
        lp = e.response.json()

    if 'error' in lp:
        if lp['error']['type'] == 'email_invalid':
            # Email address is recognized as disposable by clearbit.
            # Let's flag as populated and be done with it.
            pass

        else:
            # Retry in one minute
            return self.retry(countdown=60)

    else:
        if lp is None or lp['person'] is None:
            # We don't have a match. This shouldn't happen, ever?
            return

        # This is None if clearbit has no data on it
        profile.linkedin = lp['person']['linkedin']['handle']

        if lp['company'] is not None:
            profile.company = lp['company']['name']

    profile.autopopulated = True
    profile.save()
