from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from . import views


urlpatterns = [
    url(r'^profile$', views.profile),

    url(r'^sign-up$', views.register),

    # JWT paths
    url(r'^auth$', obtain_jwt_token),
    url(r'^auth/refresh$', refresh_jwt_token),
]
