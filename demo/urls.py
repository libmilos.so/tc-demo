from django.conf.urls import url, include
from django.contrib import admin

from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^posts/', include('posts.urls')),
    url(r'^accounts/', include('accounts.urls')),
]

urlpatterns = format_suffix_patterns(urlpatterns)


# imports 404 and 500 handlers
from .views import *
