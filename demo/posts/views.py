from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics, mixins

from .models import Post, Like
from .serializers import PostSerializer, LikeSerializer
from .views_permissions import PostPermissions


class PostListCreate(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (PostPermissions, )


class PostListByUser(generics.ListAPIView):
    serializer_class = PostSerializer

    def get_queryset(self):
        created_by = self.kwargs['user_id']
        return Post.objects.filter(created_by=created_by)


class PostLike(APIView):

    def _get_post(self, pk):
        return get_object_or_404(Post, pk=pk)

    def get(self, request, post_id):
        raise NotImplementedError

    def post(self, request, post_id):
        post = self._get_post(post_id)

        status, like = post.like(self.request.user)
        if status:
            sz = LikeSerializer(like)
            return Response(sz.data)
        return Response({'message': 'You have already liked this post.'})

    def delete(self, request, post_id):
        post = self._get_post(post_id)

        status = post.unlike(self.request.user)
        if status:
            return Response({'message': 'Success.'})
        return Response({'message': "You haven't liked this post."})
