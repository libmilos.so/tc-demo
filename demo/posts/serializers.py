from rest_framework import serializers

from accounts import serializers as acc_sz
from .models import Post, Like


class PostSerializer(serializers.ModelSerializer):
    created_by = acc_sz.BasicUserSerializer(required=False)
    num_likes = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ('id', 'text', 'created_on', 'created_by', 'num_likes', )
        read_only_fields = ('created_on', 'created_by', 'num_likes', )

    def get_num_likes(self, obj):
        return obj.like_count()


class LikeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Like
        fields = ('post', 'user', )
