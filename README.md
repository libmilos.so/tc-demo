# Le Demo
This is a REST-based social network demonstration.

### Installation
In order to run this demo, you'll need Python>=3.4 and RabbitMQ.

To get RabbitMQ, do the following on Ubuntu:
```sh
# apt-get install rabbitmq-server
# service start rabbitmq-server
```

On Arch:
```sh
# pacman -S rabbitmq
# systemctl start rabbitmq
```

For the purpose of this demo, there is a script provided for running celery worker.
```sh
$ ./scripts/celery-demo-worker.sh
```

### API documentation

You can find the swagger draft published here:
<https://app.swaggerhub.com/apis/kek6/kek/1.0.0>

### Deployment

For more info on deploying Django project and Celery workers in production environment, please refer to their corresponding docs.
Although, you really don't wanna run it in production. Just sayin'.

### Running the test bot

Before running the bot, you should init it's config and adjust it according to your needs.

```sh
$ cd bot/
$ cp settings.json.example settings.json
```

Then run it, and voila~

```sh
$ python run.py
```
